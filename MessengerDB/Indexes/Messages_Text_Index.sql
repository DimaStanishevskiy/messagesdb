﻿CREATE FULLTEXT CATALOG [MessagesCatalog] AS DEFAULT;

GO

CREATE FULLTEXT INDEX
	ON [dbo].[Messages]
		([Text])
	KEY INDEX [Messages_Id_Index]
	ON [MessagesCatalog]
	WITH CHANGE_TRACKING AUTO
