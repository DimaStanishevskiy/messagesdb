﻿CREATE VIEW [dbo].[Contacts]
	AS SELECT 
		m.From_Id as Id,
		m.To_Id as ContactId
	FROM [dbo].[Messages] m
	GROUP BY m.From_Id, m.To_Id
	UNION
	SELECT 
		m.To_Id as Id,
		m.From_Id as ContactId
	FROM [dbo].[Messages] m
	GROUP BY m.From_Id, m.To_Id