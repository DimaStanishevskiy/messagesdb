﻿CREATE TABLE [dbo].[Messages]
(
	[Id] INT NOT NULL PRIMARY KEY, 
    [From_Id] INT NOT NULL, 
    [To_Id] INT NOT NULL, 
    [Text] NVARCHAR(MAX) NOT NULL, 
    [Date] DATE NOT NULL, 
    [Time] TIME NOT NULL

    FOREIGN KEY (From_Id) REFERENCES Users(Id)
    FOREIGN KEY (To_Id) REFERENCES Users(Id)
)
